## paytient-brewery-service
#### Submitted by David J Barnes (nullableint@gmail.com) on 1/29/2021

### Run from source code:
```shell
git clone https://davidjbarnes@bitbucket.org/davidjbarnes/paytient.git
cd paytient
./gradlew clean build bootRun
```

### Run from Docker
```shell
docker run --rm -d --name paytient -p 8081:8081 davidjbarnes/paytient-brewery
```

### Access on AWS EC2 (Docker)
```shell
 http://paytient.davidjbarnes.com:8081/swagger-ui.html
```